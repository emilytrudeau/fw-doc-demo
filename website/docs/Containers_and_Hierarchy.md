---
id: Containers-and-hierarchy
title: Containers and Hierarchy  
sidebar_label: Containers and Hierarchy
slug: /Containers-and-hierarchy
---
# Flywheel Organization

In Flywheel, data is organized in "Containers". Containers are
essentially objects (as in, object-oriented programming kind of objects)
that contain actual data files, as well as metadata and custom
information about the container or files themselves.

There are 8 containers used in Flywheel: Five of them are primarily
hierarchical containers used for organization of data:

1.  Group
2.  Project
3.  Subject
4.  Session
5.  Acquisition

In these five containers, the parent/child order is rigidly enforced.
For example, a "Group" container cannot **directly** have an acquisition
container attached as a child. Instead, it must have a "Project" child,
which must have a "Subject" child, which must have a "Session" child,
which then can have an "Acquisition" child.

In addition to these five containers, there are two "sub-containers"
that can belong to most containers regardless of their level in the
hierarchy.

1.  File
2.  Analysis

Finally, there is the last container, which is used to organize running
tasks (gears) on the site:

1.  Job

Each container has its own metadata attributes associated with it. Some
are shared across multiple (or all) containers, while others are unique
to a specific container type.

Additionally, users can upload custom information to any container as a
form of additional metadata fields.

## Groups

The `Group` Container has the following
attributes:

> -   providers
> -   created
> -   editions
> -   permissions
> -   \_id
> -   modified
> -   tags
> -   label

Group containers cannot have Files, Analyses, or Jobs associated with
them.

Group containers may have child Projects:
```
Group1  
├─ Project 1 
├─ Project 2 
└─ Project 3
```

## Projects

The `Project` Container has the following attributes  
-   info
-   info\_exists
-   group
-   description
-   templates
-   providers
-   notes
-   created
-   modified
-   label
-   tags
-   parents
-   editions
-   **analyses**
-   \_id
-   public
-   **files**
-   permissions

Note that analyses and files are attributes of the 
`Project` container.

Project containers may have Files, and Analyses associated with them.

Project containers may have child Subjects:
```
Project 1  
├─ \[analyses\] 
│ └─Analysis1 
│ ├─ Analysis File1 
│ └─ Analysis File2 
│
├─ \[files\] 
│ └─ Project File1 
├─ Subject 1 
└─ Subject 2
```
## Subjects

The `Subject` Container has the following attributes:  
-   code
-   sex
-   strain
-   species
-   ethnicity
-   cohort
-   label
-   parents
-   type
-   master\_code
-   files
-   info\_exists
-   firstname
-   tags
-   lastname
-   analyses
-   permissions
-   info
-   created
-   age
-   modified
-   public
-   project
-   race
-   \_id
-   notes

Note that analyses and files are attributes of the 
`Subject` container.

The "project" attribute is not referring to a "child" Project that
belongs to the subject, it instead is a reference to the subject
container's parent project.

Subject containers may have child Sessions:
```
Subject 1  
├─ \[analyses\] 
│ └─Analysis1 
│ ├─ Analysis File1 
│ └─ Analysis File2 
│
├─ \[files\] 
│ └─ Subject File1 
├─ Session 1 
└─ Session 2
```

## Sessions

The `Session` Container has the following attributes:  
-   uid
-   weight
-   operator
-   timezone
-   project\_has\_template
-   group
-   label
-   parents
-   subject
-   public
-   files
-   info\_exists
-   tags
-   timestamp
-   analyses
-   satisfies\_template
-   permissions
-   info
-   created
-   age
-   modified
-   project
-   \_id
-   notes

Note that analyses and files are attributes of the 
`Acquisition` container.

The "subject" attribute is not referring to a "child" Subject that
belongs to the session, it instead is a reference to the sessions parent
containers.

session containers may have child files:
```
Session 1  
├─ \[analyses\] 
│ └─Analysis1 
│ ├─ Analysis File1 
│ └─ Analysis File2 
│
└─ \[files\] 
└─ Session File1
```

## Acquisitions

The `Acquisition` Container has the following attributes:  
-   info
-   files
-   uid
-   created
-   analyses
-   timestamp
-   notes
-   tags
-   collections
-   label
-   session
-   parents
-   modified
-   timezone
-   permissions
-   \_id
-   public
-   info\_exists

Note that analyses and files are attributes of the 
`Session` container.

The "subject" and "project" attributes are not referring to a "child"
Project/Subject that belongs to the session, it instead is a reference
to the sessions parent containers.

session containers may have child acquisitions:
```
Session 1  
├─ \[analyses\] 
│ └─Analysis1 
│ ├─ Analysis File1 
│ └─ Analysis File2 
│
├─ \[files\] 
│ └─ Session File1 
├─ Acquisition 1 
└─ Acquisition 2
```

The files attached to an acquisition are the files that you actually see
in flywheel on a typical session view:

![acquisition files](../static/img/AcquisitionFiles.png)

For all other containers, "files" will appear under the "Attachments"
category when viewing the container's info:

![all other files](../static/img/AllOtherFiles.png)
