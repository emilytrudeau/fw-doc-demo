module.exports = {
  someSidebar: {
    'Getting Started': ['GettingStarted', 'QuickStart'],
    'Work with Data': ['FindingData', 'CreateObjects'],
    'Data Model':['Containers-and-hierarchy','hierarchy', 'permissions', 'custom_roles', 'containers'],
    'Flywheel Views':['Dataviews'],
    'SDK Gears':['sdk_gears'],
    'Tutorials':['Upload-data-to-a-Flywheel-Project','tutorial_Update-MoCo-Acquisition-Label', 'tutorial_Job Monitoring-User-and-Developer-Version', 'tutorial_Job Monitoring-Admin-Version','tutorial_Getting-and-Reloading-Containers','tutorial_List-outdated-gear-rules', 'tutorial_edit-acquisition-timestamp', 'tutorial_edit-acquisition-timestamp','tutorial_Delete-Empty-Containers','tutorial_Batch-Run-Flywheel-Gears','tutorial_Add-subjects-to-collection','tutorial_Adding-REDCap-fields-to-Flywheel-metadata','tutorial_Flywheel-redcap-integration', 'tutorial_Flywheel-SDK-Example'],
    'Quick Reference':['QuickReference'],
  },
};
